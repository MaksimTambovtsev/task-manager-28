package ru.tsc.tambovtsev.tm.command.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.dto.Domain;
import ru.tsc.tambovtsev.tm.enumerated.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DomainLoadXMLFasterXMLCommand extends AbstractDomainCommand {

    @NotNull
    private final static String NAME = "load-xml-fasterxml";

    @NotNull
    private final static String DESCRIPTION = "Load projects, tasks and users from xml fasterxml";

    @Nullable
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(FILE_FASTERXML_XML)));
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final Domain domain = xmlMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
