package ru.tsc.tambovtsev.tm.command.domain;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.dto.Domain;
import ru.tsc.tambovtsev.tm.enumerated.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class DomainLoadXMLJaxbCommand extends AbstractDomainCommand {

    @NotNull
    private final static String NAME = "load-xml-jaxb";

    @NotNull
    private final static String DESCRIPTION = "Load projects, tasks and users from xml file";

    @NotNull
    @Override
    public String getName() { return NAME; }

    @NotNull
    @Override
    public String getDescription() { return DESCRIPTION; }

    @NotNull
    @Override
    public final Role[] getRoles() { return new Role[]{Role.ADMIN}; }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final File file = new File(FILE_JAXB_XML);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
    }

}
